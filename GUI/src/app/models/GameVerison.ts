import { PlayerEnum } from './Board';

export enum GameVersion {
    TwoPlayers,
    RedVsComputer = PlayerEnum.RED,
    YellowVsComputer = PlayerEnum.YELLOW
}