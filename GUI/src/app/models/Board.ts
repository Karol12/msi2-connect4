export class Board {
  private _fragments: BoardFragment[]
  private _possiblePosition: number[]
  private _sizeRows: number;
  private _sizeColumns: number;

  private fragmentsToWin: number;

  public get sizeRows(): number {
    return this._sizeRows;
  }

  public set sizeRows(value: number) {
    this._sizeRows = value;
  }

  public get sizeColumns(): number {
    return this._sizeColumns;
  }

  public set sizeColumns(value: number) {
    this._sizeColumns = value;
  }

  public get fragments() {
    return this._fragments
  }

  public set fragments(value: BoardFragment[]) {
    this._fragments = value
  }

  public get possiblePosition(): number[] {
    return this._possiblePosition;
  }

  public set possiblePosition(value: number[]) {
    this._possiblePosition = value;
  }

  constructor() {
    this.sizeRows = 6;
    this.sizeColumns = 7;
    this.fragmentsToWin = 4;
    this.fragments = new Array()
    this._possiblePosition = new Array()
    for (let i = 0; i < this.sizeColumns * this.sizeRows; i++) {
      this.fragments.push(new BoardFragment(Math.floor(i / this.sizeColumns), i % this.sizeColumns));
    }
    for (let i = 0; i < this.sizeColumns; i++) {
      this.possiblePosition.push(this.sizeRows - 1);
    }
  }

  getFragment(row: number, column: number) {
    return this.fragments[column + row * this.sizeColumns];
  }
  getTopFragmentFromColumn(column: number) {
    return this.getFragment(this.possiblePosition[column], column);
  }

  isEndGame(lastFragment: BoardFragment): EndGameConfiguration {
    let currentPlayer = lastFragment.belongToPlayer;

    for (let i = 0; i < this.sizeRows - this.fragmentsToWin + 1; i++) {
      let column = this.fragments.filter(x => x.column == lastFragment.column).sort((x,y)=> x.row - y.row);
      if (column.splice(i, this.fragmentsToWin).every(x => x.belongToPlayer == currentPlayer)) {
        return new EndGameConfiguration(true, currentPlayer);
      }
    }

    for (let i = 0; i < this.sizeColumns - this.fragmentsToWin + 1; i++) {
      let row = this.fragments.filter(x => x.row == lastFragment.row).sort((x,y)=> x.column - y.column);
      if (row.splice(i, this.fragmentsToWin).every(x => x.belongToPlayer == currentPlayer)) {
        return new EndGameConfiguration(true, currentPlayer);
      }
    }

    let diagonalFirst = [lastFragment];
    let diagonalSecond = [lastFragment];

    let i = lastFragment.row;
    let j = lastFragment.column;
    while (i > 0 && j > 0) {
      diagonalFirst.push(this.getFragment(--i, --j));
    }
    i = lastFragment.row;
    j = lastFragment.column;
    while (i > 0 && j < this.sizeColumns - 1) {
      diagonalSecond.push(this.getFragment(--i, ++j));
    }
    i = lastFragment.row;
    j = lastFragment.column;
    while (i < this.sizeRows - 1 && j > 0) {
      diagonalSecond.push(this.getFragment(++i, --j));
    }
    i = lastFragment.row;
    j = lastFragment.column;
    while (i < this.sizeRows - 1 && j < this.sizeColumns - 1) {
      diagonalFirst.push(this.getFragment(++i, ++j));
    }

    diagonalFirst = diagonalFirst.sort((x,y)=> x.row - y.row);
    diagonalSecond = diagonalSecond.sort((x,y)=> x.row - y.row);

    for (let i = 0; i < diagonalFirst.length - this.fragmentsToWin + 1; i++) {
      var diagonal = diagonalFirst.filter(x => true);
      if (diagonal.length < this.fragmentsToWin) continue;
      if (diagonal.splice(i, this.fragmentsToWin).every(x=> x.belongToPlayer == currentPlayer)) {
        return new EndGameConfiguration(true, currentPlayer);
      }
    }

    for (let i = 0; i < diagonalSecond.length - this.fragmentsToWin + 1; i++) {
      var diagonal = diagonalSecond.filter(x => true);
      if (diagonal.length < this.fragmentsToWin) continue;
      if (diagonal.splice(i, this.fragmentsToWin).every(x=> x.belongToPlayer == currentPlayer)) {
        return new EndGameConfiguration(true, currentPlayer);
      }
    }

    return new EndGameConfiguration(false, PlayerEnum.NO_PLAYER);
  }
}

export class BoardFragment {
  row: number;
  column: number;
  belongToPlayer: PlayerEnum;
  inPlace: boolean;

  constructor(row: number, column: number) {
    this.row = row;
    this.column = column;
    this.belongToPlayer = PlayerEnum.NO_PLAYER;
    this.inPlace = false;
  }
}

export class EndGameConfiguration {

  private _endGame: boolean;
  private _winner: PlayerEnum;

  public get endGame(): boolean {
    return this._endGame;
  }

  public set endGame(value: boolean) {
    this._endGame = value;
  }

  public get winner(): PlayerEnum {
    return this._winner;
  }

  public set winner(value: PlayerEnum) {
    this._winner = value;
  }

  constructor(isEnd: boolean, winner: PlayerEnum) {
    this.endGame = isEnd;
    this.winner = winner;
  }
}

export enum PlayerEnum {
  NO_PLAYER = 0,
  RED = -1,
  YELLOW = 1
}
