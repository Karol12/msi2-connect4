export class StartGameMessage {
    player: Number;
    timeout: Number;

    constructor(player: Number, timeout: Number) {
        this.player = player;
        this.timeout = timeout;
    }
}