import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Board } from '../models/Board';
import { async } from 'q';
import { HttpClient } from '@angular/common/http';
import { StartGameMessage } from '../models/StartGameMessage';

@Injectable({
  providedIn: 'root'
})
export class MctsSolverService {

  private playerUrl: string = 'http://127.0.0.1:5000/move';
  private startUrl: string = 'http://127.0.0.1:5000/start';

  constructor(private http: HttpClient) { }

  computerPlayerMove(board: Board): Observable<object> {
    const convertedBoard = this.convertBoard(board);

    return this.http.post(this.playerUrl, convertedBoard, { headers: { 'Content-Type': 'application/json' } });

    // return of(Math.floor(Math.random() * 6)).pipe();
  }

  startGame(oppositePlayer: Number, timeout: Number): Observable<any> {
   return this.http.post(this.startUrl, new StartGameMessage(oppositePlayer, timeout),
    { headers: { 'Content-Type': 'application/json' } });
  }

  private convertBoard(board: Board): number[][] {
    let convertedBoard: number[][] = new Array<Array<number>>();
    
    for (let i=0;i<board.sizeRows;i++) {
      let row = new Array<number>();
      for (let j=0;j<board.sizeColumns;j++) {
        row.push(0);
      }
      convertedBoard.push(row);
    }

    board.fragments.forEach(x=> {
      convertedBoard[board.sizeRows - x.row - 1][x.column] = x.belongToPlayer;
    });
    return convertedBoard;
  }
}
