import { TestBed } from '@angular/core/testing';

import { MctsSolverService } from './mcts-solver.service';

describe('MctsSolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MctsSolverService = TestBed.get(MctsSolverService);
    expect(service).toBeTruthy();
  });
});
