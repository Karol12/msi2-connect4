import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameComponentComponent } from './components/game-component/game-component.component';

const routes: Routes = [ { path:  'game', component:  GameComponentComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
