import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameComponentComponent } from './components/game-component/game-component.component';
import { BoardComponentComponent } from './components/board-component/board-component.component';
import { MctsSolverService } from './services/mcts-solver.service';

@NgModule({
  declarations: [
    AppComponent,
    GameComponentComponent,
    BoardComponentComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    MctsSolverService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
