import { Component, OnInit, Input } from '@angular/core';
import { Board, BoardFragment, PlayerEnum } from './../../models/Board'
import { debug } from 'util';
import { Observable, of } from 'rxjs';
import { Texts } from 'src/app/constans/Texts';
import { Colors } from 'src/app/constans/Colors';
import { Styles } from 'src/app/constans/Styles';
import { HostListener } from '@angular/core';
import { GameVersion } from 'src/app/models/GameVerison';
import { version } from 'punycode';
import { MctsSolverService } from 'src/app/services/mcts-solver.service';

@Component({
  selector: 'app-board-component',
  templateUrl: './board-component.component.html',
  styleUrls: ['./board-component.component.scss'],
})
export class BoardComponentComponent implements OnInit {

  @Input() gameVersion: GameVersion;
  @Input() timeout: number;
  @Input() startingPlayer: PlayerEnum;

  private board: Board;
  private sizeOfSingleElement: number;
  private _currentPlayer: number;

  public playerTurnText: string;
  public playerTurnStyle;
  private isEndGame: boolean;
  private isComputerMove: boolean;

  public get isButtonEnable(): boolean {
    return !this.isEndGame && !this.isComputerMove;
  }

  public get currentPlayer(): number {
    return this._currentPlayer;
  }

  public set currentPlayer(value: number) {
    if (value == PlayerEnum.RED) {
      this.playerTurnText = Texts.playerRedTurn;
      this.playerTurnStyle = Styles.playerRedColor;
    } else if (value == PlayerEnum.YELLOW) {
      this.playerTurnText = Texts.playerYellowTurn;
      this.playerTurnStyle = Styles.playerYellowColor;
    } else {
      this.playerTurnStyle = Styles.playerZeroColor;
    }
    this._currentPlayer = value;
  }


  constructor(private mctsSolverService: MctsSolverService) {
  }

  ngOnInit() {
    this.sizeOfSingleElement = 50;
    this.setUpGame();
  }

  setUpGame() {
    this.board = new Board();
    let boardComponent = this;
    this.currentPlayer = this.startingPlayer;
    debugger;
    if (this.gameVersion == GameVersion.TwoPlayers) {
      this.isEndGame = false;
      return;
    }
    if (this.gameVersion == this.currentPlayer) {
      this.mctsSolverService.startGame(-this.currentPlayer, this.timeout).subscribe( x=> {
        this.isEndGame = false;
      });
    } else {
      this.isComputerMove = true;
      this.mctsSolverService.startGame(this.currentPlayer, this.timeout).subscribe( x=> {
        boardComponent.computerPlayerMove();
        this.isEndGame = false;
      });
    }
  }

  positionTop(fragment: BoardFragment): number {
    return this.positionTopBasedOnNumber(fragment.row);
  }

  positionLeft(fragment: BoardFragment): number {
    return this.positionLeftBasedOnNumber(fragment.column);
  }

  positionTopBasedOnNumber(row: number): number {
    return this.sizeOfSingleElement * row;
  }

  positionLeftBasedOnNumber(column: number): number {
    return this.sizeOfSingleElement * column;
  }

  positionOfFragment(fragment: BoardFragment): number {
    if (fragment.inPlace) {
      return 0;
    }
    else {
      fragment.inPlace = true;
      return -50 - this.positionTopBasedOnNumber(fragment.row);
    }
  }

  styleOfFragemnt(fragment: BoardFragment) {
    const position = -50 - this.positionTopBasedOnNumber(fragment.row);
    if (fragment.belongToPlayer == PlayerEnum.NO_PLAYER) return { "top": `${position}px`, };
    const seconds = (fragment.row + 1) / 2;
    const color = (fragment.belongToPlayer == PlayerEnum.RED) ? Colors.playerRedColor : Colors.playerYellowColor;
    if (fragment.inPlace) {
      return {
        "-webkit-transition": `top ${seconds}s ease 0s`,
        "transition": `top ${seconds}s ease 0s`,
        "top": "0px",
        "background-color": color
      };
    } else {
      setTimeout(() => { fragment.inPlace = true }, 1000);
      return {
        "-webkit-transition": `top ${seconds}s ease 0s`,
        "transition": `top ${seconds}s ease 0s`,
        "top": `${position}px`,
        "background-color": color
      };
    }
  }

  clickToAddFragment(column: number) {
    this.addFragment(column, this.currentPlayer);
    if (!this.isEndGame) {
      this.changePlayer();
      if (this.gameVersion != GameVersion.TwoPlayers) {
        this.computerPlayerMove();
      }
    }
  }

  addFragment(column: number, playerNumber: number) {
    let fragment = this.board.getTopFragmentFromColumn(column);
    fragment.belongToPlayer = playerNumber;
    fragment.inPlace = true;
    this.board.possiblePosition[column]--;
    let isEndGame = this.board.isEndGame(fragment);
    if (isEndGame.endGame) {
      this.isEndGame = true;
      this.currentPlayer = fragment.belongToPlayer;
      this.playerTurnText = Texts.winText(this.currentPlayer);
      return;
    }
    if (this.board.possiblePosition.every(x => x < 0)) {
      this.isEndGame = true;
      this.currentPlayer = 0;
      this.playerTurnText = "Remis";
      return;
    }
  }

  redPlayerFragments() {
    return this.board.fragments.filter(x => x.belongToPlayer == PlayerEnum.RED);
  }
  yellowPlayerFragments() {
    return this.board.fragments.filter(x => x.belongToPlayer == PlayerEnum.YELLOW);
  }
  columnNumberList(): Array<number> {
    return Array.from([0, 1, 2, 3, 4, 5, 6]);
  }
  buttonEnabled(column: number) {
    return this.board.possiblePosition[column] >= 0;
  }

  changePlayer() {
    if (this.currentPlayer == PlayerEnum.RED) {
      this.currentPlayer = PlayerEnum.YELLOW;
    } else {
      this.currentPlayer = PlayerEnum.RED;
    }
  }

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(e: KeyboardEvent) {
    if ((e.keyCode >= 49 && e.keyCode <= 55)) {
      this.clickToAddFragment(e.keyCode - 49);
    } else if (e.keyCode >= 97 && e.keyCode <= 103) {
      this.clickToAddFragment(e.keyCode - 97);
    }
  }

  computerPlayerMove() {
    this.isComputerMove = true;
    let boardComponent = this;
    this.mctsSolverService.computerPlayerMove(this.board).subscribe(x => {
      boardComponent.addFragment(Number(x), boardComponent.currentPlayer);
      boardComponent.isComputerMove = false;
      if (!boardComponent.isEndGame) {
        boardComponent.changePlayer();
      }
    });
  }
}
