import { Component, OnInit } from '@angular/core';
import { GameVersion } from 'src/app/models/GameVerison';
import { Texts } from 'src/app/constans/Texts';
import { PlayerEnum } from 'src/app/models/Board';

@Component({
  selector: 'app-game-component',
  templateUrl: './game-component.component.html',
  styleUrls: ['./game-component.component.scss']
})
export class GameComponentComponent implements OnInit {

  private _version: GameVersion;
  private _startingPlayer: PlayerEnum;
  private _gameStated: boolean;
  private _timeout: number;

  private twoPlayers: string = Texts.twoPlayers;
  private redVsComputer: string = Texts.redVsComputer;
  private yellowVsComputer: string = Texts.yellowVsComputer;
  private startGameText: string = Texts.startGame;

	public get version(): GameVersion {
		return this._version;
	}
	public set version(value: GameVersion) {
		this._version = value;
	}
	public get gameStated(): boolean {
		return this._gameStated;
	}
	public set gameStated(value: boolean) {
		this._gameStated = value;
  }

	public get timeout(): number {
		return this._timeout;
	}
	public set timeout(value: number) {
		this._timeout = value;
	}
	public get startingPlayer(): PlayerEnum {
		return this._startingPlayer;
	}
	public set startingPlayer(value: PlayerEnum) {
		this._startingPlayer = value;
	}


  constructor() { 
    this.gameStated = false;
    this.version = GameVersion.RedVsComputer;
    this.startingPlayer = PlayerEnum.RED;
    this.timeout = 2;
  }

  ngOnInit() {
  }

  selectVersion(value: number) {
    this.version = value;
  }

  startGame() {
    this.gameStated = true;
  }
  
  selectStartPlayer(value: number) {
    this.startingPlayer = value;
  }

  backToMenu() {
    this.gameStated = false;
    this.version = GameVersion.RedVsComputer;
    this.startingPlayer = PlayerEnum.RED;
  }

}
