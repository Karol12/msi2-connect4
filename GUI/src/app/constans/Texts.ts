import { PlayerEnum } from '../models/Board';

export class Texts {
  static playerRedTurn: string = "Tura Gracza Czerwonego";
  static playerYellowTurn: string = "Tura Gracza Żółtego";

  static twoPlayers: string = "Dwóch graczy";
  static redVsComputer: string = "Czerwony gracz";
  static yellowVsComputer: string = "Żółty gracz";
  static startGame: string = "Zacznij grę";

  static winText(playerId): string {
    if (playerId == PlayerEnum.RED) {
      return "Wygrał gracz czerwony";
    }
    return "Wygrał gracz żółty"
  }

}
