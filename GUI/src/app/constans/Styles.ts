import { Colors } from './Colors';

export class Styles {
    static playerRedColor = {'background-color':Colors.playerRedColor} 
    static playerYellowColor = {'background-color':Colors.playerYellowColor} 
    static playerZeroColor = {'background-color':Colors.playerZeroColor} 
}