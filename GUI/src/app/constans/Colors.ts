export class Colors {
  static playerRedColor: string = 'red';
  static playerYellowColor: string = 'yellow';
  static playerZeroColor: string = 'lightgreen';
}
