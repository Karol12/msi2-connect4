(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_game_component_game_component_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/game-component/game-component.component */ "./src/app/components/game-component/game-component.component.ts");




var routes = [{ path: 'game', component: _components_game_component_game_component_component__WEBPACK_IMPORTED_MODULE_3__["GameComponentComponent"] }];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <nav>\r\n  <a routerLink=\"/game\" routerLinkActive=\"active\">Gra</a>\r\n</nav>\r\n\r\n<router-outlet></router-outlet> -->\r\n<app-game-component></app-game-component>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'GUI';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_game_component_game_component_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/game-component/game-component.component */ "./src/app/components/game-component/game-component.component.ts");
/* harmony import */ var _components_board_component_board_component_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/board-component/board-component.component */ "./src/app/components/board-component/board-component.component.ts");
/* harmony import */ var _services_mcts_solver_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/mcts-solver.service */ "./src/app/services/mcts-solver.service.ts");











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _components_game_component_game_component_component__WEBPACK_IMPORTED_MODULE_8__["GameComponentComponent"],
                _components_board_component_board_component_component__WEBPACK_IMPORTED_MODULE_9__["BoardComponentComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"]
            ],
            providers: [
                _services_mcts_solver_service__WEBPACK_IMPORTED_MODULE_10__["MctsSolverService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/board-component/board-component.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/board-component/board-component.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"turnInfoDiv\" [ngStyle]=\"this.playerTurnStyle\">\r\n    {{playerTurnText}}\r\n</div>\r\n<div class=\"buttons\" *ngIf=\"this.isButtonEnable\">\r\n    <div *ngFor=\"let column of [0,1,2,3,4,5,6]\">\r\n        <button *ngIf=\"buttonEnabled(column)\" class=\"add-fragment-button\" [style.left.px]=\"column*50\"\r\n            (click)=\"clickToAddFragment(column)\">\r\n        </button>\r\n    </div>\r\n</div>\r\n\r\n<button class=\"buttons btn btn-primary\" (click)=\"setUpGame()\" *ngIf=\"this.isEndGame\">\r\n    Nowa Gra\r\n</button>\r\n\r\n<div class=\"buttons text-center\" *ngIf=\"this.isComputerMove\">\r\n    Ruch Komputera\r\n</div>\r\n\r\n<div class=\"rectangle\">\r\n\r\n    <div *ngFor=\"let fragment of this.board.fragments\" [style.top.px]=\"positionTop(fragment)\"\r\n        [style.left.px]=\"positionLeft(fragment)\" class=\"white-dot\">\r\n        <div>\r\n            <!-- <ng-container *ngIf=\"fragment.row == 5 && fragment.column == 0 || fragment.row == 3 && fragment.column == 3\" > -->\r\n            <ng-container>\r\n                <div class=\"player-dot moving-dot\" [ngStyle]=\"styleOfFragemnt(fragment)\">\r\n                </div>\r\n            </ng-container>\r\n        </div>\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "./src/app/components/board-component/board-component.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/board-component/board-component.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dot {\n  height: 40px;\n  width: 40px;\n  top: 300px;\n  background-color: #bbb;\n  border-radius: 50%;\n  display: inline-block;\n  position: relative; }\n\n.moving-dot {\n  z-index: -1;\n  top: 0px;\n  /* Safari */\n  transition: top 2s ease 0s; }\n\n.white-dot {\n  height: 50px;\n  width: 50px;\n  background-color: #fff;\n  border-radius: 50%;\n  display: inline-block;\n  position: absolute;\n  z-index: 2; }\n\n.player-dot {\n  height: 50px;\n  width: 50px;\n  border-radius: 50%;\n  position: absolute; }\n\n@-webkit-keyframes example {\n  from {\n    top: -50px; }\n  to {\n    top: 0px; } }\n\n@keyframes example {\n  from {\n    top: -50px; }\n  to {\n    top: 0px; } }\n\n.rectangle {\n  position: relative;\n  left: 100px;\n  top: 150px;\n  height: 300px;\n  width: 350px;\n  background-color: #555;\n  color: red;\n  z-index: 1; }\n\n.add-fragment-button {\n  position: absolute;\n  background-color: greenyellow;\n  width: 50px;\n  height: 50px;\n  top: 0px; }\n\n.buttons {\n  position: relative;\n  left: 100px;\n  top: 50px;\n  width: 350px;\n  height: 50px; }\n\n.turnInfoDiv {\n  margin-top: 5%;\n  position: relative;\n  left: 100px;\n  top: 0px;\n  text-align: center;\n  width: 350px;\n  font-size: 30px; }\n\n.myCard {\n  margin-left: 25%;\n  margin-left: calc(50% - 275px);\n  width: 550px;\n  height: 600px;\n  margin-top: 5%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ib2FyZC1jb21wb25lbnQvQzpcXFVzZXJzXFxLYXJvbFxcRGVza3RvcFxcUHJvak9CXFxNU0kgMlxcUHJvamVrdDJcXG1zaTItY29ubmVjdDRcXEdVSS9zcmNcXGFwcFxcY29tcG9uZW50c1xcYm9hcmQtY29tcG9uZW50XFxib2FyZC1jb21wb25lbnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7RUFDVixzQkFBc0I7RUFDdEIsa0JBQWtCO0VBQ2xCLHFCQUFxQjtFQUNyQixrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxXQUFXO0VBQ1gsUUFBUTtFQUM0QixXQUFBO0VBQ3BDLDBCQUEwQixFQUFBOztBQUs1QjtFQUNFLFlBQVk7RUFDWixXQUFXO0VBQ1gsc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLFVBQVUsRUFBQTs7QUFHWjtFQUNFLFlBQVk7RUFDWixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFO0lBQU0sVUFBVSxFQUFBO0VBQ2hCO0lBQUksUUFBUSxFQUFBLEVBQUE7O0FBRmQ7RUFDRTtJQUFNLFVBQVUsRUFBQTtFQUNoQjtJQUFJLFFBQVEsRUFBQSxFQUFBOztBQUdkO0VBQ0Usa0JBQWtCO0VBQ2xCLFdBQVU7RUFDVixVQUFTO0VBQ1QsYUFBYTtFQUNiLFlBQVk7RUFDWixzQkFBc0I7RUFDdEIsVUFBVTtFQUNWLFVBQVUsRUFBQTs7QUFHWjtFQUNFLGtCQUFrQjtFQUNsQiw2QkFBNkI7RUFDN0IsV0FBVTtFQUNWLFlBQVc7RUFDWCxRQUFRLEVBQUE7O0FBR1Y7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVTtFQUNWLFNBQVE7RUFDUixZQUFXO0VBQ1gsWUFBVyxFQUFBOztBQUdiO0VBQ0UsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsUUFBTztFQUNQLGtCQUFrQjtFQUNsQixZQUFXO0VBQ1gsZUFBZSxFQUFBOztBQUdqQjtFQUNFLGdCQUFlO0VBQ2YsOEJBQTZCO0VBQzdCLFlBQVc7RUFDWCxhQUFZO0VBQ1osY0FDRixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9ib2FyZC1jb21wb25lbnQvYm9hcmQtY29tcG9uZW50LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRvdCB7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICB3aWR0aDogNDBweDtcclxuICAgIHRvcDogMzAwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYmJiO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIH1cclxuXHJcbiAgLm1vdmluZy1kb3Qge1xyXG4gICAgei1pbmRleDogLTE7XHJcbiAgICB0b3A6IDBweDtcclxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogdG9wIDJzIGVhc2UgMHM7IC8qIFNhZmFyaSAqL1xyXG4gICAgdHJhbnNpdGlvbjogdG9wIDJzIGVhc2UgMHM7XHJcbiAgICAvL2FuaW1hdGlvbi1uYW1lOiBleGFtcGxlO1xyXG4gICAgLy9hbmltYXRpb24tZHVyYXRpb246IDRzO1xyXG4gIH1cclxuXHJcbiAgLndoaXRlLWRvdCB7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB6LWluZGV4OiAyO1xyXG4gIH1cclxuXHJcbiAgLnBsYXllci1kb3Qge1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgfVxyXG5cclxuICBAa2V5ZnJhbWVzIGV4YW1wbGUge1xyXG4gICAgZnJvbSB7dG9wOiAtNTBweH1cclxuICAgIHRvIHt0b3A6IDBweH1cclxuICB9XHJcblxyXG4gIC5yZWN0YW5nbGUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbGVmdDoxMDBweDtcclxuICAgIHRvcDoxNTBweDtcclxuICAgIGhlaWdodDogMzAwcHg7XHJcbiAgICB3aWR0aDogMzUwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTU1O1xyXG4gICAgY29sb3I6IHJlZDtcclxuICAgIHotaW5kZXg6IDE7XHJcbiAgfVxyXG5cclxuICAuYWRkLWZyYWdtZW50LWJ1dHRvbntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGdyZWVueWVsbG93O1xyXG4gICAgd2lkdGg6NTBweDtcclxuICAgIGhlaWdodDo1MHB4O1xyXG4gICAgdG9wOiAwcHg7XHJcbiAgfVxyXG5cclxuICAuYnV0dG9uc3tcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGxlZnQ6MTAwcHg7XHJcbiAgICB0b3A6NTBweDtcclxuICAgIHdpZHRoOjM1MHB4O1xyXG4gICAgaGVpZ2h0OjUwcHg7XHJcbiAgfVxyXG5cclxuICAudHVybkluZm9EaXYge1xyXG4gICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBsZWZ0OjEwMHB4O1xyXG4gICAgdG9wOjBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHdpZHRoOjM1MHB4O1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gIH1cclxuXHJcbiAgLm15Q2FyZCB7XHJcbiAgICBtYXJnaW4tbGVmdDoyNSU7XHJcbiAgICBtYXJnaW4tbGVmdDpjYWxjKDUwJSAtIDI3NXB4KTtcclxuICAgIHdpZHRoOjU1MHB4O1xyXG4gICAgaGVpZ2h0OjYwMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogNSVcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/components/board-component/board-component.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/board-component/board-component.component.ts ***!
  \*************************************************************************/
/*! exports provided: BoardComponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardComponentComponent", function() { return BoardComponentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_Board__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../models/Board */ "./src/app/models/Board.ts");
/* harmony import */ var src_app_constans_Texts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/constans/Texts */ "./src/app/constans/Texts.ts");
/* harmony import */ var src_app_constans_Colors__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/constans/Colors */ "./src/app/constans/Colors.ts");
/* harmony import */ var src_app_constans_Styles__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/constans/Styles */ "./src/app/constans/Styles.ts");
/* harmony import */ var src_app_models_GameVerison__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/models/GameVerison */ "./src/app/models/GameVerison.ts");
/* harmony import */ var src_app_services_mcts_solver_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/mcts-solver.service */ "./src/app/services/mcts-solver.service.ts");









var BoardComponentComponent = /** @class */ (function () {
    function BoardComponentComponent(mctsSolverService) {
        this.mctsSolverService = mctsSolverService;
    }
    Object.defineProperty(BoardComponentComponent.prototype, "isButtonEnable", {
        get: function () {
            return !this.isEndGame && !this.isComputerMove;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BoardComponentComponent.prototype, "currentPlayer", {
        get: function () {
            return this._currentPlayer;
        },
        set: function (value) {
            if (value == _models_Board__WEBPACK_IMPORTED_MODULE_2__["PlayerEnum"].RED) {
                this.playerTurnText = src_app_constans_Texts__WEBPACK_IMPORTED_MODULE_3__["Texts"].playerRedTurn;
                this.playerTurnStyle = src_app_constans_Styles__WEBPACK_IMPORTED_MODULE_5__["Styles"].playerRedColor;
            }
            else if (value == _models_Board__WEBPACK_IMPORTED_MODULE_2__["PlayerEnum"].YELLOW) {
                this.playerTurnText = src_app_constans_Texts__WEBPACK_IMPORTED_MODULE_3__["Texts"].playerYellowTurn;
                this.playerTurnStyle = src_app_constans_Styles__WEBPACK_IMPORTED_MODULE_5__["Styles"].playerYellowColor;
            }
            else {
                this.playerTurnStyle = src_app_constans_Styles__WEBPACK_IMPORTED_MODULE_5__["Styles"].playerZeroColor;
            }
            this._currentPlayer = value;
        },
        enumerable: true,
        configurable: true
    });
    BoardComponentComponent.prototype.ngOnInit = function () {
        this.sizeOfSingleElement = 50;
        this.setUpGame();
    };
    BoardComponentComponent.prototype.setUpGame = function () {
        var _this = this;
        this.board = new _models_Board__WEBPACK_IMPORTED_MODULE_2__["Board"]();
        var boardComponent = this;
        this.currentPlayer = this.startingPlayer;
        debugger;
        if (this.gameVersion == src_app_models_GameVerison__WEBPACK_IMPORTED_MODULE_6__["GameVersion"].TwoPlayers) {
            this.isEndGame = false;
            return;
        }
        if (this.gameVersion == this.currentPlayer) {
            this.mctsSolverService.startGame(-this.currentPlayer, this.timeout).subscribe(function (x) {
                _this.isEndGame = false;
            });
        }
        else {
            this.isComputerMove = true;
            this.mctsSolverService.startGame(this.currentPlayer, this.timeout).subscribe(function (x) {
                boardComponent.computerPlayerMove();
                _this.isEndGame = false;
            });
        }
    };
    BoardComponentComponent.prototype.positionTop = function (fragment) {
        return this.positionTopBasedOnNumber(fragment.row);
    };
    BoardComponentComponent.prototype.positionLeft = function (fragment) {
        return this.positionLeftBasedOnNumber(fragment.column);
    };
    BoardComponentComponent.prototype.positionTopBasedOnNumber = function (row) {
        return this.sizeOfSingleElement * row;
    };
    BoardComponentComponent.prototype.positionLeftBasedOnNumber = function (column) {
        return this.sizeOfSingleElement * column;
    };
    BoardComponentComponent.prototype.positionOfFragment = function (fragment) {
        if (fragment.inPlace) {
            return 0;
        }
        else {
            fragment.inPlace = true;
            return -50 - this.positionTopBasedOnNumber(fragment.row);
        }
    };
    BoardComponentComponent.prototype.styleOfFragemnt = function (fragment) {
        var position = -50 - this.positionTopBasedOnNumber(fragment.row);
        if (fragment.belongToPlayer == _models_Board__WEBPACK_IMPORTED_MODULE_2__["PlayerEnum"].NO_PLAYER)
            return { "top": position + "px", };
        var seconds = (fragment.row + 1) / 2;
        var color = (fragment.belongToPlayer == _models_Board__WEBPACK_IMPORTED_MODULE_2__["PlayerEnum"].RED) ? src_app_constans_Colors__WEBPACK_IMPORTED_MODULE_4__["Colors"].playerRedColor : src_app_constans_Colors__WEBPACK_IMPORTED_MODULE_4__["Colors"].playerYellowColor;
        if (fragment.inPlace) {
            return {
                "-webkit-transition": "top " + seconds + "s ease 0s",
                "transition": "top " + seconds + "s ease 0s",
                "top": "0px",
                "background-color": color
            };
        }
        else {
            setTimeout(function () { fragment.inPlace = true; }, 1000);
            return {
                "-webkit-transition": "top " + seconds + "s ease 0s",
                "transition": "top " + seconds + "s ease 0s",
                "top": position + "px",
                "background-color": color
            };
        }
    };
    BoardComponentComponent.prototype.clickToAddFragment = function (column) {
        this.addFragment(column, this.currentPlayer);
        if (!this.isEndGame) {
            this.changePlayer();
            if (this.gameVersion != src_app_models_GameVerison__WEBPACK_IMPORTED_MODULE_6__["GameVersion"].TwoPlayers) {
                this.computerPlayerMove();
            }
        }
    };
    BoardComponentComponent.prototype.addFragment = function (column, playerNumber) {
        var fragment = this.board.getTopFragmentFromColumn(column);
        fragment.belongToPlayer = playerNumber;
        fragment.inPlace = true;
        this.board.possiblePosition[column]--;
        var isEndGame = this.board.isEndGame(fragment);
        if (isEndGame.endGame) {
            this.isEndGame = true;
            this.currentPlayer = fragment.belongToPlayer;
            this.playerTurnText = src_app_constans_Texts__WEBPACK_IMPORTED_MODULE_3__["Texts"].winText(this.currentPlayer);
            return;
        }
        if (this.board.possiblePosition.every(function (x) { return x < 0; })) {
            this.isEndGame = true;
            this.currentPlayer = 0;
            this.playerTurnText = "Remis";
            return;
        }
    };
    BoardComponentComponent.prototype.redPlayerFragments = function () {
        return this.board.fragments.filter(function (x) { return x.belongToPlayer == _models_Board__WEBPACK_IMPORTED_MODULE_2__["PlayerEnum"].RED; });
    };
    BoardComponentComponent.prototype.yellowPlayerFragments = function () {
        return this.board.fragments.filter(function (x) { return x.belongToPlayer == _models_Board__WEBPACK_IMPORTED_MODULE_2__["PlayerEnum"].YELLOW; });
    };
    BoardComponentComponent.prototype.columnNumberList = function () {
        return Array.from([0, 1, 2, 3, 4, 5, 6]);
    };
    BoardComponentComponent.prototype.buttonEnabled = function (column) {
        return this.board.possiblePosition[column] >= 0;
    };
    BoardComponentComponent.prototype.changePlayer = function () {
        if (this.currentPlayer == _models_Board__WEBPACK_IMPORTED_MODULE_2__["PlayerEnum"].RED) {
            this.currentPlayer = _models_Board__WEBPACK_IMPORTED_MODULE_2__["PlayerEnum"].YELLOW;
        }
        else {
            this.currentPlayer = _models_Board__WEBPACK_IMPORTED_MODULE_2__["PlayerEnum"].RED;
        }
    };
    BoardComponentComponent.prototype.handleKeyboardEvent = function (e) {
        if ((e.keyCode >= 49 && e.keyCode <= 55)) {
            this.clickToAddFragment(e.keyCode - 49);
        }
        else if (e.keyCode >= 97 && e.keyCode <= 103) {
            this.clickToAddFragment(e.keyCode - 97);
        }
    };
    BoardComponentComponent.prototype.computerPlayerMove = function () {
        this.isComputerMove = true;
        var boardComponent = this;
        this.mctsSolverService.computerPlayerMove(this.board).subscribe(function (x) {
            boardComponent.addFragment(Number(x), boardComponent.currentPlayer);
            boardComponent.isComputerMove = false;
            if (!boardComponent.isEndGame) {
                boardComponent.changePlayer();
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], BoardComponentComponent.prototype, "gameVersion", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], BoardComponentComponent.prototype, "timeout", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], BoardComponentComponent.prototype, "startingPlayer", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:keypress', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [KeyboardEvent]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], BoardComponentComponent.prototype, "handleKeyboardEvent", null);
    BoardComponentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-board-component',
            template: __webpack_require__(/*! ./board-component.component.html */ "./src/app/components/board-component/board-component.component.html"),
            styles: [__webpack_require__(/*! ./board-component.component.scss */ "./src/app/components/board-component/board-component.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_mcts_solver_service__WEBPACK_IMPORTED_MODULE_7__["MctsSolverService"]])
    ], BoardComponentComponent);
    return BoardComponentComponent;
}());



/***/ }),

/***/ "./src/app/components/game-component/game-component.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/game-component/game-component.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"this.gameStated\" class=\"card myCard\">\r\n    <button type=\"button\" class=\"btn btn-primary backButton\" (click)=\"backToMenu()\">Wróć do Menu</button>\r\n\r\n    <app-board-component [gameVersion]=\"this.version\" [timeout]=\"this.timeout\" [startingPlayer]=\"this.startingPlayer\">\r\n\r\n    </app-board-component>\r\n</div>\r\n\r\n\r\n<div *ngIf=\"!this.gameStated\" class=\"card cardBground text-center\" style=\"width: 30%; margin-left: 35%; margin-top: 5%\">\r\n     <div class=\"buttons\">\r\n\r\n        <label class=\"mainLabel\">Wybierz tryb gry:</label>\r\n\r\n        <div>\r\n            <input type=\"radio\" id=\"twoPlayers\" name=\"gameVersionRadios\" (change)=\"selectVersion(0)\">\r\n            <label for=\"twoPlayers\">{{this.twoPlayers}}</label>\r\n        </div>\r\n\r\n        <div>\r\n            <input type=\"radio\" id=\"redPlayer\" name=\"gameVersionRadios\" checked (change)=\"selectVersion(-1)\">\r\n            <label for=\"redPlayer\">{{this.redVsComputer}}</label>\r\n        </div>\r\n\r\n        <div>\r\n            <input type=\"radio\" id=\"yellowPlayer\" name=\"gameVersionRadios\" (change)=\"selectVersion(1)\">\r\n            <label for=\"yellowPlayer\">{{this.yellowVsComputer}}</label>\r\n        </div>\r\n\r\n        <div>\r\n            <label for=\"timeoutInput\">Tiemout:</label>\r\n            <input [(ngModel)]=\"timeout\" id=\"timeoutInput\" type=\"number\" min=1 step=\"0.1\">\r\n        </div>\r\n\r\n        <label class=\"mainLabel\">Rozpoczynający gracz:</label>\r\n        \r\n        <div>\r\n            <input type=\"radio\" id=\"yellowStart\" name=\"startingColorRadios\" (change)=\"selectStartPlayer(1)\">\r\n            <label for=\"yellowStart\">Żółty</label>\r\n        </div>\r\n\r\n        <div>\r\n            <input type=\"radio\" id=\"redStart\" name=\"startingColorRadios\" (change)=\"selectStartPlayer(-1)\" checked>\r\n            <label for=\"redStart\">Czerwony</label>\r\n        </div>\r\n\r\n        <button type=\"button\" class=\"btn btn-primary myButton\" (click)=\"startGame()\">\r\n            {{this.startGameText}}\r\n        </button>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/game-component/game-component.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/components/game-component/game-component.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".buttons {\n  margin: 5%;\n  display: inline-grid;\n  align-content: center; }\n\n.myButton {\n  align-self: center;\n  width: 100%;\n  padding: 20px; }\n\n.mainLabel {\n  margin: 0; }\n\nlabel {\n  margin: 15px;\n  font-size: 20px; }\n\ninput[type=radio] {\n  /* IE 9 */\n  -webkit-transform: scale(2);\n  /* Chrome, Safari, Opera */\n  transform: scale(2); }\n\ninput[type=number] {\n  width: 10%; }\n\n.myCard {\n  margin-left: 25%;\n  margin-left: calc(50% - 275px);\n  width: 550px;\n  height: 600px;\n  margin-top: 5%; }\n\n.backButton {\n  position: fixed;\n  top: 10px;\n  left: 10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9nYW1lLWNvbXBvbmVudC9DOlxcVXNlcnNcXEthcm9sXFxEZXNrdG9wXFxQcm9qT0JcXE1TSSAyXFxQcm9qZWt0MlxcbXNpMi1jb25uZWN0NFxcR1VJL3NyY1xcYXBwXFxjb21wb25lbnRzXFxnYW1lLWNvbXBvbmVudFxcZ2FtZS1jb21wb25lbnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxVQUFVO0VBQ1Ysb0JBQW9CO0VBQ3BCLHFCQUFxQixFQUFBOztBQUd6QjtFQUNJLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsYUFBYSxFQUFBOztBQUdqQjtFQUNJLFNBQVMsRUFBQTs7QUFHYjtFQUNJLFlBQVk7RUFDWixlQUFlLEVBQUE7O0FBR25CO0VBQzZCLFNBQUE7RUFDekIsMkJBQTJCO0VBQUUsMEJBQUE7RUFDN0IsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksVUFBUyxFQUFBOztBQUdiO0VBQ0ksZ0JBQWU7RUFDZiw4QkFBNkI7RUFDN0IsWUFBVztFQUNYLGFBQVk7RUFDWixjQUFjLEVBQUE7O0FBR2xCO0VBQ0ksZUFBZTtFQUNmLFNBQVE7RUFDUixVQUFTLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2dhbWUtY29tcG9uZW50L2dhbWUtY29tcG9uZW50LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ1dHRvbnMge1xyXG4gICAgbWFyZ2luOiA1JTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ncmlkO1xyXG4gICAgYWxpZ24tY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG4ubXlCdXR0b24ge1xyXG4gICAgYWxpZ24tc2VsZjogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAyMHB4O1xyXG59XHJcblxyXG4ubWFpbkxhYmVsIHtcclxuICAgIG1hcmdpbjogMDtcclxufVxyXG5cclxubGFiZWwge1xyXG4gICAgbWFyZ2luOiAxNXB4O1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPXJhZGlvXSB7XHJcbiAgICAtbXMtdHJhbnNmb3JtOiBzY2FsZSgyKTsgLyogSUUgOSAqL1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDIpOyAvKiBDaHJvbWUsIFNhZmFyaSwgT3BlcmEgKi9cclxuICAgIHRyYW5zZm9ybTogc2NhbGUoMik7XHJcbn1cclxuXHJcbmlucHV0W3R5cGU9bnVtYmVyXSB7XHJcbiAgICB3aWR0aDoxMCU7XHJcbn1cclxuXHJcbi5teUNhcmQge1xyXG4gICAgbWFyZ2luLWxlZnQ6MjUlO1xyXG4gICAgbWFyZ2luLWxlZnQ6Y2FsYyg1MCUgLSAyNzVweCk7XHJcbiAgICB3aWR0aDo1NTBweDtcclxuICAgIGhlaWdodDo2MDBweDtcclxuICAgIG1hcmdpbi10b3A6IDUlO1xyXG4gIH1cclxuXHJcbi5iYWNrQnV0dG9ue1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgdG9wOjEwcHg7XHJcbiAgICBsZWZ0OjEwcHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/game-component/game-component.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/game-component/game-component.component.ts ***!
  \***********************************************************************/
/*! exports provided: GameComponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameComponentComponent", function() { return GameComponentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_models_GameVerison__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/models/GameVerison */ "./src/app/models/GameVerison.ts");
/* harmony import */ var src_app_constans_Texts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/constans/Texts */ "./src/app/constans/Texts.ts");
/* harmony import */ var src_app_models_Board__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/models/Board */ "./src/app/models/Board.ts");





var GameComponentComponent = /** @class */ (function () {
    function GameComponentComponent() {
        this.twoPlayers = src_app_constans_Texts__WEBPACK_IMPORTED_MODULE_3__["Texts"].twoPlayers;
        this.redVsComputer = src_app_constans_Texts__WEBPACK_IMPORTED_MODULE_3__["Texts"].redVsComputer;
        this.yellowVsComputer = src_app_constans_Texts__WEBPACK_IMPORTED_MODULE_3__["Texts"].yellowVsComputer;
        this.startGameText = src_app_constans_Texts__WEBPACK_IMPORTED_MODULE_3__["Texts"].startGame;
        this.gameStated = false;
        this.version = src_app_models_GameVerison__WEBPACK_IMPORTED_MODULE_2__["GameVersion"].RedVsComputer;
        this.startingPlayer = src_app_models_Board__WEBPACK_IMPORTED_MODULE_4__["PlayerEnum"].RED;
        this.timeout = 2;
    }
    Object.defineProperty(GameComponentComponent.prototype, "version", {
        get: function () {
            return this._version;
        },
        set: function (value) {
            this._version = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentComponent.prototype, "gameStated", {
        get: function () {
            return this._gameStated;
        },
        set: function (value) {
            this._gameStated = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentComponent.prototype, "timeout", {
        get: function () {
            return this._timeout;
        },
        set: function (value) {
            this._timeout = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameComponentComponent.prototype, "startingPlayer", {
        get: function () {
            return this._startingPlayer;
        },
        set: function (value) {
            this._startingPlayer = value;
        },
        enumerable: true,
        configurable: true
    });
    GameComponentComponent.prototype.ngOnInit = function () {
    };
    GameComponentComponent.prototype.selectVersion = function (value) {
        this.version = value;
    };
    GameComponentComponent.prototype.startGame = function () {
        this.gameStated = true;
    };
    GameComponentComponent.prototype.selectStartPlayer = function (value) {
        this.startingPlayer = value;
    };
    GameComponentComponent.prototype.backToMenu = function () {
        this.gameStated = false;
        this.version = src_app_models_GameVerison__WEBPACK_IMPORTED_MODULE_2__["GameVersion"].RedVsComputer;
        this.startingPlayer = src_app_models_Board__WEBPACK_IMPORTED_MODULE_4__["PlayerEnum"].RED;
    };
    GameComponentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-game-component',
            template: __webpack_require__(/*! ./game-component.component.html */ "./src/app/components/game-component/game-component.component.html"),
            styles: [__webpack_require__(/*! ./game-component.component.scss */ "./src/app/components/game-component/game-component.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], GameComponentComponent);
    return GameComponentComponent;
}());



/***/ }),

/***/ "./src/app/constans/Colors.ts":
/*!************************************!*\
  !*** ./src/app/constans/Colors.ts ***!
  \************************************/
/*! exports provided: Colors */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Colors", function() { return Colors; });
var Colors = /** @class */ (function () {
    function Colors() {
    }
    Colors.playerRedColor = 'red';
    Colors.playerYellowColor = 'yellow';
    Colors.playerZeroColor = 'lightgreen';
    return Colors;
}());



/***/ }),

/***/ "./src/app/constans/Styles.ts":
/*!************************************!*\
  !*** ./src/app/constans/Styles.ts ***!
  \************************************/
/*! exports provided: Styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Styles", function() { return Styles; });
/* harmony import */ var _Colors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Colors */ "./src/app/constans/Colors.ts");

var Styles = /** @class */ (function () {
    function Styles() {
    }
    Styles.playerRedColor = { 'background-color': _Colors__WEBPACK_IMPORTED_MODULE_0__["Colors"].playerRedColor };
    Styles.playerYellowColor = { 'background-color': _Colors__WEBPACK_IMPORTED_MODULE_0__["Colors"].playerYellowColor };
    Styles.playerZeroColor = { 'background-color': _Colors__WEBPACK_IMPORTED_MODULE_0__["Colors"].playerZeroColor };
    return Styles;
}());



/***/ }),

/***/ "./src/app/constans/Texts.ts":
/*!***********************************!*\
  !*** ./src/app/constans/Texts.ts ***!
  \***********************************/
/*! exports provided: Texts */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Texts", function() { return Texts; });
/* harmony import */ var _models_Board__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../models/Board */ "./src/app/models/Board.ts");

var Texts = /** @class */ (function () {
    function Texts() {
    }
    Texts.winText = function (playerId) {
        if (playerId == _models_Board__WEBPACK_IMPORTED_MODULE_0__["PlayerEnum"].RED) {
            return "Wygrał gracz czerwony";
        }
        return "Wygrał gracz żółty";
    };
    Texts.playerRedTurn = "Tura Gracza Czerwonego";
    Texts.playerYellowTurn = "Tura Gracza Żółtego";
    Texts.twoPlayers = "Dwóch graczy";
    Texts.redVsComputer = "Czerwony gracz";
    Texts.yellowVsComputer = "Żółty gracz";
    Texts.startGame = "Zacznij grę";
    return Texts;
}());



/***/ }),

/***/ "./src/app/models/Board.ts":
/*!*********************************!*\
  !*** ./src/app/models/Board.ts ***!
  \*********************************/
/*! exports provided: Board, BoardFragment, EndGameConfiguration, PlayerEnum */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Board", function() { return Board; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardFragment", function() { return BoardFragment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EndGameConfiguration", function() { return EndGameConfiguration; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerEnum", function() { return PlayerEnum; });
var Board = /** @class */ (function () {
    function Board() {
        this.sizeRows = 6;
        this.sizeColumns = 7;
        this.fragmentsToWin = 4;
        this.fragments = new Array();
        this._possiblePosition = new Array();
        for (var i = 0; i < this.sizeColumns * this.sizeRows; i++) {
            this.fragments.push(new BoardFragment(Math.floor(i / this.sizeColumns), i % this.sizeColumns));
        }
        for (var i = 0; i < this.sizeColumns; i++) {
            this.possiblePosition.push(this.sizeRows - 1);
        }
    }
    Object.defineProperty(Board.prototype, "sizeRows", {
        get: function () {
            return this._sizeRows;
        },
        set: function (value) {
            this._sizeRows = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Board.prototype, "sizeColumns", {
        get: function () {
            return this._sizeColumns;
        },
        set: function (value) {
            this._sizeColumns = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Board.prototype, "fragments", {
        get: function () {
            return this._fragments;
        },
        set: function (value) {
            this._fragments = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Board.prototype, "possiblePosition", {
        get: function () {
            return this._possiblePosition;
        },
        set: function (value) {
            this._possiblePosition = value;
        },
        enumerable: true,
        configurable: true
    });
    Board.prototype.getFragment = function (row, column) {
        return this.fragments[column + row * this.sizeColumns];
    };
    Board.prototype.getTopFragmentFromColumn = function (column) {
        return this.getFragment(this.possiblePosition[column], column);
    };
    Board.prototype.isEndGame = function (lastFragment) {
        var currentPlayer = lastFragment.belongToPlayer;
        for (var i_1 = 0; i_1 < this.sizeRows - this.fragmentsToWin + 1; i_1++) {
            var column = this.fragments.filter(function (x) { return x.column == lastFragment.column; }).sort(function (x, y) { return x.row - y.row; });
            if (column.splice(i_1, this.fragmentsToWin).every(function (x) { return x.belongToPlayer == currentPlayer; })) {
                return new EndGameConfiguration(true, currentPlayer);
            }
        }
        for (var i_2 = 0; i_2 < this.sizeColumns - this.fragmentsToWin + 1; i_2++) {
            var row = this.fragments.filter(function (x) { return x.row == lastFragment.row; }).sort(function (x, y) { return x.column - y.column; });
            if (row.splice(i_2, this.fragmentsToWin).every(function (x) { return x.belongToPlayer == currentPlayer; })) {
                return new EndGameConfiguration(true, currentPlayer);
            }
        }
        var diagonalFirst = [lastFragment];
        var diagonalSecond = [lastFragment];
        var i = lastFragment.row;
        var j = lastFragment.column;
        while (i > 0 && j > 0) {
            diagonalFirst.push(this.getFragment(--i, --j));
        }
        i = lastFragment.row;
        j = lastFragment.column;
        while (i > 0 && j < this.sizeColumns - 1) {
            diagonalSecond.push(this.getFragment(--i, ++j));
        }
        i = lastFragment.row;
        j = lastFragment.column;
        while (i < this.sizeRows - 1 && j > 0) {
            diagonalSecond.push(this.getFragment(++i, --j));
        }
        i = lastFragment.row;
        j = lastFragment.column;
        while (i < this.sizeRows - 1 && j < this.sizeColumns - 1) {
            diagonalFirst.push(this.getFragment(++i, ++j));
        }
        diagonalFirst = diagonalFirst.sort(function (x, y) { return x.row - y.row; });
        diagonalSecond = diagonalSecond.sort(function (x, y) { return x.row - y.row; });
        for (var i_3 = 0; i_3 < diagonalFirst.length - this.fragmentsToWin + 1; i_3++) {
            var diagonal = diagonalFirst.filter(function (x) { return true; });
            if (diagonal.length < this.fragmentsToWin)
                continue;
            if (diagonal.splice(i_3, this.fragmentsToWin).every(function (x) { return x.belongToPlayer == currentPlayer; })) {
                return new EndGameConfiguration(true, currentPlayer);
            }
        }
        for (var i_4 = 0; i_4 < diagonalSecond.length - this.fragmentsToWin + 1; i_4++) {
            var diagonal = diagonalSecond.filter(function (x) { return true; });
            if (diagonal.length < this.fragmentsToWin)
                continue;
            if (diagonal.splice(i_4, this.fragmentsToWin).every(function (x) { return x.belongToPlayer == currentPlayer; })) {
                return new EndGameConfiguration(true, currentPlayer);
            }
        }
        return new EndGameConfiguration(false, PlayerEnum.NO_PLAYER);
    };
    return Board;
}());

var BoardFragment = /** @class */ (function () {
    function BoardFragment(row, column) {
        this.row = row;
        this.column = column;
        this.belongToPlayer = PlayerEnum.NO_PLAYER;
        this.inPlace = false;
    }
    return BoardFragment;
}());

var EndGameConfiguration = /** @class */ (function () {
    function EndGameConfiguration(isEnd, winner) {
        this.endGame = isEnd;
        this.winner = winner;
    }
    Object.defineProperty(EndGameConfiguration.prototype, "endGame", {
        get: function () {
            return this._endGame;
        },
        set: function (value) {
            this._endGame = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EndGameConfiguration.prototype, "winner", {
        get: function () {
            return this._winner;
        },
        set: function (value) {
            this._winner = value;
        },
        enumerable: true,
        configurable: true
    });
    return EndGameConfiguration;
}());

var PlayerEnum;
(function (PlayerEnum) {
    PlayerEnum[PlayerEnum["NO_PLAYER"] = 0] = "NO_PLAYER";
    PlayerEnum[PlayerEnum["RED"] = -1] = "RED";
    PlayerEnum[PlayerEnum["YELLOW"] = 1] = "YELLOW";
})(PlayerEnum || (PlayerEnum = {}));


/***/ }),

/***/ "./src/app/models/GameVerison.ts":
/*!***************************************!*\
  !*** ./src/app/models/GameVerison.ts ***!
  \***************************************/
/*! exports provided: GameVersion */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameVersion", function() { return GameVersion; });
var GameVersion;
(function (GameVersion) {
    GameVersion[GameVersion["TwoPlayers"] = 0] = "TwoPlayers";
    GameVersion[GameVersion["RedVsComputer"] = -1] = "RedVsComputer";
    GameVersion[GameVersion["YellowVsComputer"] = 1] = "YellowVsComputer";
})(GameVersion || (GameVersion = {}));


/***/ }),

/***/ "./src/app/models/StartGameMessage.ts":
/*!********************************************!*\
  !*** ./src/app/models/StartGameMessage.ts ***!
  \********************************************/
/*! exports provided: StartGameMessage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartGameMessage", function() { return StartGameMessage; });
var StartGameMessage = /** @class */ (function () {
    function StartGameMessage(player, timeout) {
        this.player = player;
        this.timeout = timeout;
    }
    return StartGameMessage;
}());



/***/ }),

/***/ "./src/app/services/mcts-solver.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/mcts-solver.service.ts ***!
  \*************************************************/
/*! exports provided: MctsSolverService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MctsSolverService", function() { return MctsSolverService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _models_StartGameMessage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/StartGameMessage */ "./src/app/models/StartGameMessage.ts");




var MctsSolverService = /** @class */ (function () {
    function MctsSolverService(http) {
        this.http = http;
        this.playerUrl = 'http://127.0.0.1:5000/move';
        this.startUrl = 'http://127.0.0.1:5000/start';
    }
    MctsSolverService.prototype.computerPlayerMove = function (board) {
        var convertedBoard = this.convertBoard(board);
        return this.http.post(this.playerUrl, convertedBoard, { headers: { 'Content-Type': 'application/json' } });
        // return of(Math.floor(Math.random() * 6)).pipe();
    };
    MctsSolverService.prototype.startGame = function (oppositePlayer, timeout) {
        return this.http.post(this.startUrl, new _models_StartGameMessage__WEBPACK_IMPORTED_MODULE_3__["StartGameMessage"](oppositePlayer, timeout), { headers: { 'Content-Type': 'application/json' } });
    };
    MctsSolverService.prototype.convertBoard = function (board) {
        var convertedBoard = new Array();
        for (var i = 0; i < board.sizeRows; i++) {
            var row = new Array();
            for (var j = 0; j < board.sizeColumns; j++) {
                row.push(0);
            }
            convertedBoard.push(row);
        }
        board.fragments.forEach(function (x) {
            convertedBoard[board.sizeRows - x.row - 1][x.column] = x.belongToPlayer;
        });
        return convertedBoard;
    };
    MctsSolverService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], MctsSolverService);
    return MctsSolverService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Karol\Desktop\ProjOB\MSI 2\Projekt2\msi2-connect4\GUI\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map