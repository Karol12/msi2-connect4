Do poprawnego działania aplikacji wymagana jest przeglądarka Google Chrome lub Fifeox.
Pozostałe przeglądarki mogą wyświetlać nie poprzewnie GUI lub powodować błędy.

Z aplikacji można korzystać na dwa sposoby:

1. Uruchamiając plik index.html z folderu build

2. Uruchamiając aplikacje korzystając z node.js-a. 
Jest to bardziej skomplikowany proces, który został opisany poniżej.

Przed przystąpienie do uruchomienia należy pobrać Node.js (https://nodejs.org/en/).

Dodatkowo należy zainstalować npm (https://www.npmjs.com/products) aby możliwe było uruchomienie aplikacji.

Po zainstalowaniu dwóch powyzszych aplikacji należy z wiersza poleceń uruchomić:
npm install -g @angular/cli

Następnie w wierszu poleceń należy przejśc do folderu z aplikacją.

Kolejnym krokiem jest uruchomienie polecenia npm install, które to pobierze potrzebne pakiery.

Następnie z folderu z aplikacją uruchomić polecenie ng serve. Wówczas aplikacja zostanie uruchomiona.

Po uruchomieniu aplikacji dostępna będzie ona pod adresem http://localhost:4200/.


