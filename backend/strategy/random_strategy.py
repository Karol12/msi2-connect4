from .strategy import Strategy
from game.game_state import GameState
from numpy import random


class RandomStrategy(Strategy):

    def perform_action(self, state: GameState):
        legal_actions = state.get_legal_actions()
        action = legal_actions[random.randint(len(legal_actions))]
        return state.move(action)
