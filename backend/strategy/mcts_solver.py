from .mcts_uct import MCTS_UCT
from numpy import infty, all, abs
from enums.player import Player


class MCTS_Solver(MCTS_UCT):

    def _select(self, node):
        children = [child for child in node.children if abs(
            child.results[node.state.next_player]) != infty]
        if len(children) == 0:
            children = node.children
        return self._get_best_child_ucb(node, children, self.c)

    def _backpropagate(self, node, winner, child=None):
        node.visit()
        if winner != Player.NONE:
            if node.is_terminal_node:
                node.results[winner] = infty
                node.results[winner.opponent] = -infty
            elif child != None and child.results[winner] == infty:
                if node.state.next_player == winner:
                    node.results[winner] = infty
                    node.results[winner.opponent] = -infty
                    node.distance_to_terminate = child.distance_to_terminate + 1
                elif all([c.results[winner] == infty for c in node.children]):
                    node.results[winner] = infty
                    node.results[winner.opponent] = -infty
                    node.distance_to_terminate = child.distance_to_terminate + 1
            else:
                node.results[winner] += 1
        if node.parent != None:
            self._backpropagate(node.parent, winner, node)
