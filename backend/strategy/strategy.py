from abc import ABC, abstractmethod
from game.game_state import GameState


class Strategy(ABC):

    @abstractmethod
    def perform_action(self, state: GameState):
        pass
