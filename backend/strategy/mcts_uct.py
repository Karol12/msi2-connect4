from .strategy import Strategy
from game.node import Node
from game.game_state import GameState, N_COLS, Action
from threading import Thread, Event
from random import randint
from numpy import sqrt, log, argmin, argmax, argwhere, array_equal, max, array
import time


class MCTS_UCT(Strategy):

    def __init__(self, timeout=1, c=0):
        self.c = c
        self.timeout = timeout
        self.root = None

    def perform_action(self, state: GameState):
        self.root = self._get_new_root(state)
        if self.root == None:
            self.root = Node(state)
        self.root.parent = None
        self.run_search()
        if state.is_initial:
            mid_column = int(N_COLS / 2)
            best_child = Node(state.move(
                Action(state.next_player.disc, mid_column)), None, mid_column)
        else:
            best_child = self._get_best_child_ucb(
                self.root, self.root.children)
            best_child.parent = None
        self.root = best_child
        return best_child.state, best_child.changed_column

    def run_search(self):
        start = time.time()
        end = start
        while end - start < self.timeout:
            self._search()
            end = time.time()

    def _search(self):
        node = self._tree_policy()
        result = self._perform_monte_carlo_simulation(node)
        self._backpropagate(node, result)

    def _tree_policy(self):
        current_node = self.root
        while not current_node.is_terminal_node:
            if not current_node.is_fully_expanded:
                return current_node.expand()
            else:
                current_node = self._select(current_node)
        return current_node

    def _select(self, node):
        return self._get_best_child_ucb(node, node.children, self.c)

    def _get_best_child_ucb(self, node, children, c=0):
        children_eval = [
            child.q / child.n + c * sqrt(2 * log(node.n) / child.n)
            for child in children]
        max_eval = max(children_eval)
        best_children = [children[int(i)] for i in argwhere(
            children_eval == max_eval)]

        if(max_eval > 0):
            return best_children[argmin([child.distance_to_terminate for child in best_children])]
        else:
            return best_children[argmax([child.distance_to_terminate for child in best_children])]

    def _perform_monte_carlo_simulation(self, start_node):
        current_state = start_node.state
        while not current_state.is_game_over:
            legal_actions = current_state.get_legal_actions()
            action = legal_actions[randint(0, len(legal_actions) - 1)]
            current_state = current_state.move(action)
        return current_state.game_result

    def _backpropagate(self, node, winner, child=None):
        node.visit()
        node.results[winner] += 1.
        if node.parent != None:
            self._backpropagate(node.parent, winner)

    def _get_new_root(self, state: GameState):
        node = self.root
        if node == None:
            return None
        if array_equal(node.state.board, state.board):
            return node
        for child in node.children:
            if array_equal(child.state.board, state.board):
                return child
        return None
