import numpy as np
from termcolor import colored
from .win_configurations import WIN_CONFIGURATIONS
from enums.player import Player
from enums.disc import Disc
from .action import Action

CCC = 4  # Connected components count
N_COLS = 7
N_ROWS = 6


class GameState:
    '''
    Class representing Connect Four game state

    '''

    def __init__(self, board, next_player):
        self.board = board
        self.next_player = next_player
        self.column_summit = self._calculate_column_summit(board)
        self.disc_count = sum(self.column_summit)

    def _calculate_column_summit(self, board):
        column_summit = np.zeros(N_COLS, dtype=int)
        for col in range(0, N_COLS):
            row = 0
            while row < N_ROWS and board[row, col] != Disc.NONE:
                column_summit[col] += 1
                row += 1
        return column_summit

    @property
    def game_result(self):
        for start_row in range(N_ROWS - CCC + 1):
            for start_col in range(N_COLS - CCC + 1):
                for conf in WIN_CONFIGURATIONS:
                    val = np.multiply(self._get_window(
                        start_row, start_col), conf).sum()
                    if val == CCC * Player.YELLOW:
                        return Player.YELLOW
                    elif val == CCC * Player.RED:
                        return Player.RED
        return Player.NONE if self.disc_count == N_COLS * N_ROWS else None

    @property
    def is_game_over(self):
        return self.game_result != None

    @property
    def is_initial(self):
        return np.sum(self.column_summit) == 0

    def move(self, move):
        if move.col < 0 or move.col >= N_COLS:
            raise ValueError("Illegal move")
        row = self.column_summit[move.col]
        if row >= N_ROWS:
            raise ValueError("Illegal move")

        new_board = np.copy(self.board)
        new_board[row, move.col] = move.disc
        new_next_player = self.next_player.opponent
        return GameState(new_board, new_next_player)

    def get_legal_actions(self):
        return [Action(self.next_player.disc, col) for col in range(N_COLS) if self.column_summit[col] < N_ROWS]

    def _get_window(self, start_row, start_col):
        return self.board[start_row: start_row + CCC, start_col: start_col + CCC]

    def __str__(self):
        result = 'Winner: ' + str(self.game_result) + '\n'
        for row in [N_ROWS - 1 - x for x in range(N_ROWS)]:
            for col in range(N_COLS):
                if self.board[row, col] == Disc.RED:
                    result += colored(u"\u25CF", 'red')
                elif self.board[row, col] == Disc.YELLOW:
                    result += colored(u"\u25CF", 'yellow')
                else:
                    result += u"\u25CB"
                result += ' '
            result += '\n'
        return result
