from .game_state import GameState
from collections import defaultdict
from random import randint
from numpy import infty


class Node:

    def __init__(self, state: GameState, parent = None, changed_column = None):
        self.state = state
        self.parent = parent
        self.children = []
        self._visits_count = 0
        self.results = defaultdict(int)
        self.changed_column = changed_column
        self.distance_to_terminate = infty
        if state.is_game_over:
            self.distance_to_terminate = 0

    @property
    def untried_actions(self):
        if not hasattr(self, '_untried_actions'):
            self._untried_actions = self.state.get_legal_actions()
        return self._untried_actions

    @property
    def q(self):
        wins = self.results[self.parent.state.next_player]
        loses = self.results[-1 * self.parent.state.next_player]
        return wins - loses

    @property
    def n(self):
        return self._visits_count

    def expand(self, random: bool = False):
        if random:
            action = self.untried_actions[randint(
                0, len(self.untried_actions) - 1)]
        else:
            action = self.untried_actions.pop()
        next_state = self.state.move(action)
        child_node = Node(next_state, self, action.col)
        self.children.append(child_node)
        return child_node

    @property
    def is_fully_expanded(self):
        return len(self.untried_actions) == 0

    @property
    def is_terminal_node(self):
        return self.state.is_game_over

    def visit(self):
        self._visits_count += 1
