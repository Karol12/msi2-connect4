import colorama
from .game_state import GameState, N_COLS, N_ROWS
import numpy as np


class Game:

    def __init__(self, players, first_player):
        self.players = players
        self.first_player = first_player
        colorama.init()

    def run(self):
        state = GameState(np.zeros((N_ROWS, N_COLS), dtype=int),
                          self.first_player)
        print(state)
        while not state.is_game_over:
            player = self.players[state.next_player]
            state, _ = player.perform_action(state)
            print(state)
        return state.game_result
