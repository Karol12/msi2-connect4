from flask import Flask, request, jsonify, g
from flask_cors import CORS
import numpy as np
from game.game_state import GameState
from enums.player import Player
from strategy.mcts_solver import MCTS_Solver
import colorama
from threading import Thread

colorama.init()
app = Flask(__name__)
CORS(app)
app.secret_key = 'dljsaklqk24e21cjn!Ew@@dsa5'

c_param = 1.4
config = {
    "strategy": None,
    "player": None,
    "search_thread": None
}

def after_this_request(func):
    if not hasattr(g, 'call_after_request'):
        g.call_after_request = []
    g.call_after_request.append(func)
    return func


@app.after_request
def per_request_callbacks(response):
    for func in getattr(g, 'call_after_request', ()):
        response = func(response)
    return response

@app.route('/')
def index():
    return "Hello, World!"

@app.route('/start', methods=['POST'])
def start():
    params = request.get_json()
    config["strategy"] = MCTS_Solver(params["timeout"], c_param)
    if params["player"] == -1:
        config["player"] = Player.RED
        player_color = "red"
    else:
        config["player"] = Player.YELLOW
        player_color = "yellow"
    print("Player {0} started with timeout {1} sec".format(player_color, params["timeout"]))
    return jsonify()


@app.route('/move', methods=['POST'])
def getMove():
    @after_this_request
    def run_extra_search(response):
        thread = Thread(target=config["strategy"].run_search)
        thread.start()
        config["search_thread"] = thread
        return response
        
    if config["search_thread"] != None:
        config["search_thread"].join()
    player = config["player"]
    strategy = config["strategy"]
    board = np.matrix(request.get_json(), dtype=int)
    game_state = GameState(board, player)
    print(game_state)
    new_game_state, move = strategy.perform_action(game_state)
    print(new_game_state)
    return jsonify(move)


if __name__ == '__main__':
    app.run(debug=True)
