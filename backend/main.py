from game.game import Game
from enums.player import Player
from strategy.mcts_uct import MCTS_UCT
from strategy.mcts_solver import MCTS_Solver

players = {
    Player.YELLOW: MCTS_Solver(1, 1.4),
    Player.RED: MCTS_Solver(1, 1.4)
}

game = Game(players, Player.YELLOW)

game.run()
