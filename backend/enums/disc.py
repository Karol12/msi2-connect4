from enum import IntEnum


class Disc(IntEnum):
    '''
    Enum class representing disc used in Connect Four game

    '''
    YELLOW = 1
    RED = -1
    NONE = 0
