from enum import IntEnum
from .disc import Disc


class Player(IntEnum):
    '''
    Enum class representing player used in Connect Four game

    '''
    YELLOW = 1
    RED = -1
    NONE = 0

    @property
    def disc(self):
        return player_disc_map[self]

    @property
    def opponent(self):
        return Player.RED if self == Player.YELLOW else Player.YELLOW


player_disc_map = {
    Player.YELLOW: Disc.YELLOW,
    Player.RED: Disc.RED,
    Player.NONE: Disc.NONE
}
